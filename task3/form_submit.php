<?php
$host = "db";
$db_name = "dev_to";
$username = "root";
$password = "my-secret-pw";
$connection = new mysqli($host,  $username, $password, $db_name);

function saveData($name, $email, $message){
  global $connection;
  $name=htmlspecialchars(strip_tags($name));
  $email=htmlspecialchars(strip_tags($email));
  $message=htmlspecialchars(strip_tags($message));

  $query = "INSERT INTO test(name, email, message) VALUES('$name', '$email', '$message')";

  $callToDb = $connection->prepare($query);

  if($callToDb->execute()){
    return '<h3 style="text-align:center;">We will get back to you very shortly!</h3>';
  }
}

if( isset($_POST['submit'])){
  $name = htmlentities($_POST['name']);
  $email = htmlentities($_POST['email']);
  $message = htmlentities($_POST['message']);

  //then you can use them in a PHP function. 
  $result = saveData($name, $email, $message);
  echo $result;
}
else{
  echo '<h3 style="text-align:center;">A very detailed error message ( ͡° ͜ʖ ͡°)</h3>';
}
?>

